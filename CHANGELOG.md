# 1.0.0 - 2024-01-26
We will now use the language server embedded in the `dev` module instead of having a separate `@webhare/language-server` project

# 0.1.7 - 2021-12-10

## Things that are nice to know
- Add 'Organize LOADLIBs' Source Action for HareScript files
- Add Format Document support for supported XML files


# 0.1.6 - 2021-05-31

## Things that are nice to know
- Make server debug loglevel configurable
- Switch to using `@webhare/language-server`


# 0.1.5 - 2021-05-25

## Things that are nice to know
- XML file diagnostics
- Add some snippets


# 0.1.4 - 2021-05-12

## Things that are nice to know
- Package updates
- Add show stack trace command
- Make language server module configurable
- Add option to enable/disabled documentation diagnostics


# 0.1.0 - 2021-04-15

Initial release

## Things that are nice to know
- HareScript syntax highlighting and file diagnostics
