#!/bin/bash
cd "${BASH_SOURCE%/*}/.." || exit 1

if ! hash code ; then
  echo "'code' not found in path. Is VSCode installed?" >&2
  exit
fi

npm install
node_modules/.bin/vsce package -o /tmp/webhare.vsix && code --install-extension /tmp/webhare.vsix
rm /tmp/webhare.vsix
